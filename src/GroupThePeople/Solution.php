<?php

namespace LeetCode\GroupThePeople;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer[] $groupSizes
     * @return Integer[][]
     */
    function groupThePeople($groupSizes) 
    {
        $numbersGrouped = 0;
        $groupSize = 1;
        $result = [];

        while($numbersGrouped !== count($groupSizes)) {
            $group = [];

            for ($i = 0; $i < count($groupSizes); $i++) {
                if ($groupSizes[$i] === $groupSize) {
                    $group[] = $i;
                    $numbersGrouped++;
                    
                    if (count($group) === $groupSize) {
                        $result[] = $group;
                        $group = [];
                    }
                }
            }

            $groupSize++;
        }

        return $result;
    }

    public function runTests()
    {
        var_dump($this->groupThePeople([3,3,3,3,3,1,3]));
        echo '<br>';
        echo '<br>';
        
        var_dump($this->groupThePeople([2,1,3,3,3,2]));
        echo '<br>';
        echo '<br>';
    }
}