<?php

namespace LeetCode\Trie;

use LeetCode\AbstractSolution;

class Trie extends AbstractSolution
{
    private $words = [];
  
    /**
     * Inserts a word into the trie.
     * @param String $word
     * @return NULL
     */
    public function insert($word) 
    {
        $this->words[] = $word;
    }
  
    /**
     * Returns if the word is in the trie.
     * @param String $word
     * @return Boolean
     */
    public function search($word) 
    {
        foreach($this->words as $w) {
            if ($this->isWordEqualInLength($word, $w)) {
                $equalWords = $this->testWordEquivalence($word, $w, strlen($word) - 1);
                if ($equalWords) {
                    return true;
                }
            }
        }

        return false;
    }
  
    /**
     * Returns if there is any word in the trie that starts with the given prefix.
     * @param String $prefix
     * @return Boolean
     */
    public function startsWith($prefix) 
    {
        foreach($this->words as $word) {
            $equalWords = $this->testWordEquivalence($prefix, $word, strlen($prefix) - 1);
            if ($equalWords) {
                return true;
            }
        }

        return false;
    }

    private function isWordEqualInLength($word1, $word2)
    {
        return strlen($word1) === strlen($word2);
    }

    private function testWordEquivalence($word1, $word2, $charactedIndex)
    {
        $counter = 0;
        while ($counter <= $charactedIndex) {
            if ($word1[$counter] !== $word2[$counter]) {
                return false;
            }
            $counter++;
        }
        return true;
    }

    // private function testWordEquivalence($word1, $word2, $charactedIndex)
    // {
    //     if ($charactedIndex === 0) {
    //         return $word1[$charactedIndex] === $word2[$charactedIndex];
    //     } else {
    //         return $word1[$charactedIndex] === $word2[$charactedIndex] && $this->testWordEquivalence($word1, $word2, $charactedIndex - 1);
    //     }
    // }

    public function runTests()
    {
        $this->insert('apple');
        var_dump($this->search('apple'));
        var_dump($this->startsWith('app'));
    }
}