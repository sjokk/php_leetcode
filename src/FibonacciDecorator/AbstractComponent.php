<?php

namespace LeetCode\FibonacciDecorator;

abstract class AbstractComponent
{
    abstract public function calculate(): int;
}