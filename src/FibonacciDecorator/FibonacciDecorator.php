<?php

namespace LeetCode\FibonacciDecorator;

use LeetCode\FibonacciDecorator\AbstractComponent;

class FibonacciDecorator extends AbstractComponent
{
    private $nMinusOne;
    private $nMinusTwo;

    public function __construct(AbstractComponent $nMinusOne, AbstractComponent $nMinusTwo)
    {
        $this->nMinusOne = $nMinusOne;
        $this->nMinusTwo = $nMinusTwo;
    }

    public function calculate(): int
    {
        return $this->nMinusOne->calculate() + $this->nMinusTwo->calculate();
    }
}