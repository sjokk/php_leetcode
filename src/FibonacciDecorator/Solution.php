<?php

namespace LeetCode\FibonacciDecorator;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    public function fib(int $N): int 
    {
        $nMinusTwo = new FibonacciZero();
        $nMinusOne = new FibonacciOne();

        if ($N === 0) {
            return $nMinusTwo->calculate(); 
        } elseif ($N === 1) {
            return $nMinusOne->calculate();
        } else {
            do {
                $fibonacci = new FibonacciDecorator($nMinusOne, $nMinusTwo);
                
                $nMinusTwo = $nMinusOne;
                $nMinusOne = $fibonacci;
                $N--;
            } while ($N >= 2);
            return $fibonacci->calculate();
        }
    }

    public function runTests()
    {
        for($i = 0; $i <= 20; $i++) {
            echo 'Fib of ' . $i . ' = ' . $this->fib($i);
            echo '<br/>';
        }
    }
}