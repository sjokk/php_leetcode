<?php

namespace LeetCode\RotateArray;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer[] $nums
     * @param Integer $k
     * @return NULL
     */
    function rotate(&$nums, $k) 
    {
        $length = count($nums);
        $numberOfRotations = $k % $length;
        $count = 0;

        while ($count < $numberOfRotations) {
            $item = array_pop($nums);
            array_unshift($nums, $item);
            $count++;
        }
    }

    public function runTests()
    {
        $inputs = [
            [[1,2,3,4,5,6,7], 3],
            [[-1,-100,3,99], 2]
        ];

        foreach($inputs as $input) {
            $this->rotate($input[0], $input[1]);
            var_dump($input[0]);
            echo '</br>';
        }
    }
}