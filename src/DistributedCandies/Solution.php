<?php

namespace LeetCode\DistributedCandies;

class Solution 
{

    /**
     * @param Integer $candies
     * @param Integer $num_people
     * @return Integer[]
     */
    function distributeCandies($candies, $num_people) 
    {
        $candiesHandedOut = [];
        $count = 0;
        
        while ($candies > 0) {
            $index = $count % $num_people;
            $count++;
            
            if (! isset($candiesHandedOut[$index])) {
                $candiesHandedOut[$index] = $count;
            } else {
                $candiesHandedOut[$index] += $count;
            }
            
            $candies -= $count;
        }
        
        $candiesHandedOut[$index] += $candies;
        
        while (count($candiesHandedOut) < $num_people) {
            $candiesHandedOut[] = 0;
        }
        
        return $candiesHandedOut;
    }
}