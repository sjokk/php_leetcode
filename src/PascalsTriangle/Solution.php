<?php
namespace LeetCode\PascalsTriangle;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer $numRows
     * @return Integer[][]
     */
    function generate(int $numRows): array
    {
        $result = [];
        if ($numRows > 0) {
            // Voor elk niveau willen we de integers genereren.
            // Hiervoor hebben we de huidige array nodig thans het niveau er boven als deze bestaat
            // De eerste index en laatste index bestaan altijd uit waarden 1 voor dat niveau.
            // Elk ander niveau met meer dan 2 elementen, heeft een cell met waarde n - 1 + n van het voorgaande niveua 
            for ($row = 0; $row < $numRows; $row++) {
                $result[] = $this->generateLevel($row, $result);
            }
        }
        return $result;
    }

    private function generateLevel(int $row, array $currentTriangle): array
    {
        $result = [];
        for ($i = 0; $i <= $row; $i++) {
            if ($i > 0 && $i < $row) {
                $previousRow = $currentTriangle[$row - 1];
                $result[] = $previousRow[$i - 1] + $previousRow[$i];
            } else {
                $result[] = 1;
            }
        }
        return $result;
    }

    public function runTests()
    {
        include __DIR__ . '/input.php';
        foreach($inputs as $input) {
            $result = $this->generate($input);
            print_r($result);
            echo '<br/>';
            echo '<br/>';
        }
    }
}