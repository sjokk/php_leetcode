<?php
namespace LeetCode;

abstract class AbstractSolution
{
    abstract public function runTests();
}