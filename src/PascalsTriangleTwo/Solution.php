<?php
namespace LeetCode\PascalsTriangleTwo;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    private $triangle = [
        [1],
        [1, 1],
    ];

    /**
     * @param Integer $rowIndex
     * @return Integer[]
     */
    function getRow(int $rowIndex): array 
    {
        if ($rowIndex > 1) {
            for ($row = 2; $row <= $rowIndex; $row++) {
                $this->triangle[] = $this->generateLevel($row);
            }
        }
        return $this->triangle[$rowIndex];
    }

    private function generateLevel(int $row): array
    {
        $result = [1];
        for ($i = 1; $i <= $row; $i++) {
            if ($i > 0 && $i < $row) {
                $previousRow = $this->triangle[$row - 1];
                $result[] = $previousRow[$i - 1] + $previousRow[$i];
            }
        }
        $result[] = 1;
        return $result;
    }

    public function runTests()
    {
        include __DIR__ . '/input.php';

        foreach($inputs as $input) {
            $result = $this->getRow($input);
            print_r($result);
            echo '<br/>';
            echo '<br/>';
        }
    }
}