<?php

namespace LeetCode\DailyTemperatures;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    private $resultArray;

    /**
     * @param Integer[] $temperatures
     * @return Integer[]
     */
    public function dailyTemperatures($temperatures)
    {
        $this->resultArray = [];

        for($i = 0; $i < count($temperatures); $i++) {
            $this->resultArray[$i] = 0;
            $exhausted = true;
            $j = $i + 1;

            while ($j < count($temperatures)) {
                $this->resultArray[$i]++;
                
                if ($temperatures[$i] < $temperatures[$j]) {
                    $exhausted = false;
                    break;
                }

                $j++;
            }

            if ($exhausted) {
                $this->resultArray[$i] = 0;
            }
        }
        // If we exhaust our search without finding a temp higher,
        // we add 0 on the result array.

        // we can skip the 100s as these are the max and thus we can put 0
        // in its position.

        // The length of the temperatures array should be at least 2
        return $this->resultArray;
    }

    // Recursive implementation RIP, was a nice one but too slow.
    private function countDaysRecursively(int $original, int $index, array $temperatures, $result = 0)
    {
        // there is no temperature higher.
        if ($original === 100) {
            return 0;
        } else if ($index >= count($temperatures)) {
            return 0;
        } else {
            if ($original < $temperatures[$index]) {
                return 1 + $result;
            } else {
                return $this->countDays($original, $index + 1, $temperatures, $result + 1);
            }
        }
    }

    public function runTests()
    {
        $t = [73, 74, 75, 71, 69, 72, 76, 73];
        //$t = array_fill(0, 30000, 47);
        var_dump($this->dailyTemperatures($t));
    }
}