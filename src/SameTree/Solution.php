<?php

namespace LeetCode\SameTree;

use LeetCode\AbstractSolution;

// Definition for a binary tree node.
class TreeNode
{
    public $val = null;
    public $left = null;
    public $right = null;
    
    function __construct($value) 
    { 
        $this->val = $value; 
    }
}
 
class Solution extends AbstractSolution 
{
    /**
     * @param TreeNode $p
     * @param TreeNode $q
     * @return Boolean
     */
    function isSameTree($p, $q) 
    {
        $pStack = [];
        $qStack = [];
        
        $this->processNode($p, $pStack);
        $this->processNode($q, $qStack);
        
        return $pStack === $qStack;
    }

    private function processNode($node, &$result)
    {
        $result[] = $node->val;
        if ($this->isLeaf($node)) {
            return;
        } else {
            if ($node->left === null) {
                $result[] = null;
            } else {
                $this->processNode($node->left, $result);
            }
            if ($node->right !== null) {
                $this->processNode($node->right, $result);
            }
        }
    }

    private function isLeaf($node)
    {
        return $node->left === null && $node->right === null;
    }

    public function runTests()
    {
        $p = new TreeNode(1);
        $p->left = new TreeNode(2);
        $p->right = new TreeNode(1);

        $q = new TreeNode(1);
        $q->left = new TreeNode(1);
        $q->right = new TreeNode(2);

        var_dump($this->isSameTree($p, $q));
    }
}