<?php

require_once __DIR__ . '/vendor/autoload.php';

use LeetCode\RemoveElement\Solution;

$solution = new Solution();
$solution->runTests();