<?php

namespace Leetcode\AddandSearchWords;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    private $words = [];
  
    /**
     * Adds a word into the data structure.
     * @param String $word
     * @return NULL
     */
    function addWord($word) 
    {
        $length = strlen($word);
        $this->words[$length][] = $word;
    }
  
    /**
     * Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
     * @param String $word
     * @return Boolean
     */
    function search($word) 
    {
        // get all the words that match the number of characters of word
        $length = strlen($word);
        $words = $this->words[$length];
        // foreach word, test if it matches the pattern.

    }

    public function runTests()
    {
        return true;
    }
}