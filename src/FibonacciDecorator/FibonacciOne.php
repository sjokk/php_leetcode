<?php

namespace LeetCode\FibonacciDecorator;

use LeetCode\FibonacciDecorator\AbstractComponent;

class FibonacciOne extends AbstractComponent
{
    public function calculate(): int
    {
        return 1;
    }
}