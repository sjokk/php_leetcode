<?php

namespace LeetCode\DetectCapital;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param String $word
     * @return Boolean
     */
    function detectCapitalUse($word) 
    {
        $upperCaseCount = 0;
        $lowerCaseCount = 0;
        $firstCharUpperCase = false;

        for($i = 0; $i < strlen($word); $i++) {
            $char = $word[$i];

            if (ctype_upper($char)) {
                if ($i === 0) {
                    $firstCharUpperCase = true;
                }
                $upperCaseCount++;
            } else {
                $lowerCaseCount++;
            }
        }

        return $upperCaseCount === strlen($word) ||
                $lowerCaseCount === strlen($word) ||
                ($firstCharUpperCase && $upperCaseCount === 1);
    }

    public function runTests()
    {
        var_dump($this->detectCapitalUse('USA'));
        echo '<br>';

        var_dump($this->detectCapitalUse('FlaG'));
        echo '<br>';

        var_dump($this->detectCapitalUse('leetcode'));
        echo '<br>';

        var_dump($this->detectCapitalUse('Google'));
        echo '<br>';
    }
}