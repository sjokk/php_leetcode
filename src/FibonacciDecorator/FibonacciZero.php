<?php

namespace LeetCode\FibonacciDecorator;

use LeetCode\FibonacciDecorator\AbstractComponent;

class FibonacciZero extends AbstractComponent
{
    public function calculate(): int
    {
        return 0;
    }
}