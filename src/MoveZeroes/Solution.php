<?php

namespace LeetCode\MoveZeroes;

use LeetCode\AbstractSolution;

class Solution
{
    /**
     * @param Integer[] $nums
     * @return NULL
     */
    function moveZeroes(&$nums) 
    {
        $finalIndex = count($nums) - 1;

        for ($i = $finalIndex; $i >= 0; $i--) {
            if ($nums[$i] === 0) {
                $targetIndex = $i;
                while (
                    $targetIndex + 1 <= $finalIndex && 
                    !($nums[$targetIndex + 1] === 0)
                ) {
                    $temp = $nums[$targetIndex + 1];
                    $nums[$targetIndex + 1] = $nums[$targetIndex];
                    $nums[$targetIndex] = $temp;

                    $targetIndex++;
                }
            }
        }
    }

    // private function smartSolution(&$nums) {
    //     $size = count($nums);
    //     $pos = $size;
    //     for($i=0;$i < $size ; $i++){
    //         if($nums[$i]===0){
    //             unset($nums[$i]);
    //             $nums[$pos++]=0;
    //         }
    //     }
    // }

    public function runTests()
    {
        $nums = [0,1,0,3,12];
        $this->moveZeroes($nums);
        var_dump($nums);
        echo '<br>';
        echo '<br>';
    }
}