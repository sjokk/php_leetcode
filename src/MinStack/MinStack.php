<?php

namespace LeetCode\MinStack;

use LeetCode\AbstractSolution;

class MinStack implements AbstractSolution
{
    private $minimum = null;
    private $stack = [];
    private $top = null;

    public function push(int $element)
    {
        $this->stack[] = $element;
        $this->setTop();
        $this->setMinimum();
    }

    public function pop()
    {
        if (!$this->isEmpty()) {
            array_pop($this->stack);
            $this->setTop();
            $this->setMinimum();
        }
    }

    public function top()
    {
        return $this->top;
    }

    public function getMin()
    {
        if (!$this->isEmpty()) {
            return $this->minimum;
        }
    }

    private function setMinimum()
    {
        $this->minimum = null;

        if (!$this->isEmpty()) {
            $min = $this->stack[0];
            foreach($this->stack as $element) {
                if ($element < $min) {
                    $min = $element;
                }
            }
            $this->minimum = $min;
        }
    }

    private function setTop()
    {
        $this->top = null;

        if (!$this->isEmpty()) {
            $lastIndex = count($this->stack) - 1;
            $this->top = $this->stack[$lastIndex];
        }
    }

    private function isEmpty(): bool
    {
        return count($this->stack) === 0;
    }

    public function runTests()
    {
        return true;
    }
}