<?php
namespace LeetCode\ReverseLinkedList;

class ListNode 
{
    public $val = 0;
    public $next = null;
    
    public function __construct(int $val)
    { 
        $this->val = $val; 
    }

    public function __toString()
    {
        $result = $this->val .  '->';
        if ($this->next === null) {
            $result .= 'NULL';
        }
        return $result;
    }
}