<?php

namespace LeetCode\ValidParentheses;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    const PARENTHESES = [
        '(' => ')',
        '[' => ']',
        '{' => '}',
    ];

    private $openingParentheses = [];

    /**
     * @param String $s
     * @return Boolean
     */
    public function isValid($s) 
    {
        $index = 0;
        $stringLength = strlen($s);
        $stringIsStillValid = true;

        while ($stringIsStillValid && $index < $stringLength) {
            $parenth = $s[$index];
            $stringIsStillValid = $this->processParenth($parenth);
            $index++;
        }

        if ($stringIsStillValid && count($this->openingParentheses) === 0) {
            return true;
        } else {
            $this->openingParentheses = [];
            return false;
        }
    }

    private function processParenth(string $parenth): bool
    {
        if (array_key_exists($parenth, self::PARENTHESES)) {
            $this->openingParentheses[] = $parenth;
            return true;
        } else {
            if (in_array($parenth, self::PARENTHESES)) {
                return $this->isValidClosingParenth($parenth);
            }
        }
    }

    private function isValidClosingParenth(string $closingParenth)
    {
        if ($this->isEmpty()) {
            return false;
        } else {
            $openingParenth = array_pop($this->openingParentheses);
            return self::PARENTHESES[$openingParenth] === $closingParenth;
        }
    }

    private function isEmpty()
    {
        return count($this->openingParentheses) === 0;
    }

    public function runTests()
    {
        include __DIR__ . '/inputs.php';

        foreach($inputs as $input) {
            echo "Test Input: $input";
            
            echo '</br>';
            
            $result = $this->isValid($input) == true ? 'true' : 'false';
            echo "Result: $result";
            
            echo '</br>';
            echo '</br>';
        }
    }
}