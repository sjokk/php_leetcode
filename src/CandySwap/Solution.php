<?php
namespace LeetCode\CandySwap;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer[] $A
     * @param Integer[] $B
     * @return Integer[]
     */
    public function fairCandySwap(array $A, array $B): array 
    {
        $totalCandyA = $this->getCandyTotal($A);
        $totalCandyB = $this->getCandyTotal($B);
        foreach($A as $candyA) {
            foreach ($B as $candyB) {
                $adjustedTotalA = $totalCandyA - $candyA + $candyB;
                $adjustedTotalB = $totalCandyB - $candyB + $candyA;
                if ($adjustedTotalA === $adjustedTotalB) {
                    return [$candyA, $candyB];
                }
            }
        }
        return [];
    }

    private function getCandyTotal(array $candyBars): int
    {
        $total = 0;
        foreach($candyBars as $candyBar) {
            $total += $candyBar;
        }
        return $total;
    }

    public function __toString(): string
    {
        return 'This is the CandySwap solution class';
    }

    public function runTests()
    {
        include __DIR__ . '/input.php';

        foreach($inputs as $input) {
            $A = $input['A'];
            $B = $input['B'];
            
            print_r($input);
            $result = $this->fairCandySwap($A, $B);
            print_r($result);
            echo '<br/>';
            echo '<br/>';
        }
    }
}