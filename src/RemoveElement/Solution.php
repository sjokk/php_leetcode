<?php

namespace LeetCode\RemoveElement;

use  LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer[] $nums
     * @param Integer $val
     * @return Integer
     */
    function removeElement(&$nums, $val) {
        $nums = array_filter($nums, function ($num) use ($val) {
            return $num !== $val;
        });
        
        return count($nums);
    }

    public function runTests()
    {
        $nums = [3,2,2,3];
        $result = $this->removeElement($nums, 3);
        
        var_dump($nums);
        echo '<br>';
        echo $result;
        echo '<br>';
    }
}