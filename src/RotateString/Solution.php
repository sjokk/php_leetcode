<?php

namespace LeetCode\RotateString;

class Solution 
{
    /**
     * @param String $A
     * @param String $B
     * @return Boolean
     */
    function rotateString($A, $B) 
    {
        $count = 0;
        
        do {
            if ($A === $B) {
                return true;
            }
            
            $A = $this->shiftString($A);
            $count++;
        } while ($count < strlen($A));
        
        return false;
    }

    private function shiftString(string $s)
    {
        return substr($s, 1) . $s[0];
    }
}