<?php

namespace LeetCode\StraightLine;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer[][] $coordinates
     * @return Boolean
     */
    function checkStraightLine(array $coordinates): bool
    {
        $start = 0;
        $next = 1;
        $previousSlope = null;

        do {
            $currentSlope = $this->calculateSlope($start, $next, $coordinates);
            
            if ($previousSlope !== null && $currentSlope !== $previousSlope) {
                return false;
            }
            
            $previousSlope = $currentSlope;
            $next++;
        } while ($next < count($coordinates));
        
        return true;
    }

    private function calculateSlope(int $index, int $otherIndex, array $coordinates)
    {
        $changeInY = $this->change($coordinates[$index][1], $coordinates[$otherIndex][1]);
        $changeInX = $this->change($coordinates[$index][0], $coordinates[$otherIndex][0]);

        return $changeInY / $changeInX;
    }

    private function change(int $a, int $otherA)
    {
        return $otherA - $a;
    }

    public function runTests()
    {
        $inputs = include __DIR__ . '/input.php';
        
        foreach($inputs as $coordinates) {
            $result = $this->checkStraightLine($coordinates);
            $output = $result ? "" : " not";
            
            echo "Line is:{$output} straight";
            echo '<br>';
        }
    }
}