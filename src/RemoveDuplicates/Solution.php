<?php

namespace LeetCode\RemoveDuplicates;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    private $stack = [];

    public function removeDuplicates($S)
    {
        // put the string onto a stack
        // if the two items are identical,
        // remove them both
        // return the concatenation of
        // the stack at the end.
        $this->stack = [];
        $length = strlen($S);
        $index = -1;

        for($i = 0; $i < $length; $i++) {
            if ($index === -1 || $S[$i] !== $this->stack[$index]) {
                $this->stack[] = $S[$i];
                $index++;
            } else {
                array_pop($this->stack);
                $index--;
            }
        }

        return implode($this->stack);
    }

    public function runTests()
    {
        $result = $this->removeDuplicates('abbaca');
        echo $result;
    }
}