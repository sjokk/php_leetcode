<?php
namespace LeetCode\Fibonacci;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    private $cache = [0 , 1];

    /**
     * @param Integer $N
     * @return Integer
     */
    public function fib(int $N): int 
    {
        if ($N < 2 || array_key_exists($N, $this->cache)) {
            return $this->cache[$N];
        } else {
            $result = $this->fib($N - 1) + $this->fib($N - 2);
            $this->cache[] = $result;
            return $result;
        }
    }
}