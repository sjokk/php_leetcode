<?php

namespace LeetCode\RandomListNode;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    private $head;
    /**
     * @param head The linked list's head.
     *  Note that the head is guaranteed to be not null, so it contains at least one node.
     * @param ListNode $head
     */
    function __construct($head) 
    {
        $this->head = $head;
    }
  
    /**
     * Returns a random node's value.
     * @return Integer
     */
    function getRandom() 
    {
        $length = 0;
        $list = [];
        $current = $this->head;

        while ($current !== null) {
            $list[] = $current->val;
            $length++;
            
            $current = $current->next;
        }

        $choice = rand(0, $length - 1);

        return $list[$choice];
    }

    public function runTests()
    {
        echo $this->getRandom();
        echo '</br>';
        echo $this->getRandom();
        echo '</br>';
        echo $this->getRandom();
        echo '</br>';
    }
}