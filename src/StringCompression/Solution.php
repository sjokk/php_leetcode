<?php
namespace LeetCode\StringCompression;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{

    /**
     * @param String[] $chars
     * @return Integer
     */
    function compress(&$chars) 
    {
        $compressedChars = [];

        if (count($chars) === 1) {
            $compressedChars[] = $chars[0];
        } else {
            $charCount = 1;
            for ($i = 0; $i < count($chars); $i++) {
                if ($this->isEqualChar($chars, $i)) {
                    $charCount++;
                } else {
                    $compressedChars[] = $chars[$i];
                    if ($charCount > 1) {
                        while ($charCount >= 10) {
                            $digit = intdiv($charCount, 10);
                            $compressedChars[] = "$digit";
                            $charCount = $charCount % 10;;
                        }
                        $compressedChars[] = "$charCount";
                    }
                    $charCount = 1;
                }
            }
        }
        $chars = $compressedChars;
        return count($compressedChars);
    }

    private function isEqualChar(array $chars, int $index): bool
    {
        return $index + 1 < count($chars) && $chars[$index] === $chars[$index + 1];
    }

    public function runTests()
    {
        include __DIR__ . '/input.php';

        foreach($inputs as $input) {
            echo $this->compress($input);
            echo '</br>';
            print_r($input);
            echo '</br>';
        }
    }
}