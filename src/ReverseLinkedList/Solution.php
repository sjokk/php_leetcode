<?php
namespace LeetCode\ReverseLinkedList;

use LeetCode\AbstractSolution;
use LeetCode\ReverseLinkedList\ListNode;

class Solution extends AbstractSolution
{
    private $head = null;
    /**
     * @param ListNode $head
     * @return ListNode
     */
    public function reverseList(ListNode $head): ListNode
    {
        if ($head !== null) {
            $currentNode = $head;
            if ($currentNode !== null) {
                // Make a call to the recursive method.
                $currentNode = $this->getReversedNodes($currentNode);
                $head->next = null;
            }
            return $this->head;
        }
    }

    private function getReversedNodes(ListNode $currentNode)
    {
        if ($currentNode->next === null) {
            $this->head = $currentNode;
            return $currentNode;
        } else {
            $result = $this->getReversedNodes($currentNode->next);
            $result->next = $currentNode;
            return $currentNode;
        }
    }

    public function runTests()
    {
        include __DIR__ . '/input.php';

        $currentNode = $input;
        echo 'INPUT:';
        echo '<br/>';
        while ($currentNode !== null) {
            echo $currentNode;
            $currentNode = $currentNode->next;
        }
        echo '<br/>';
        echo '<br/>';

        $currentNode = $this->reverseList($input);
        echo 'OUTPUT:';
        echo '<br/>';
        while ($currentNode !== null) {
            echo $currentNode;
            $currentNode = $currentNode->next;
        }
    }
}