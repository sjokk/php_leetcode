<?php

namespace LeetCode\DefangIPAddress;

class Solution 
{
    /**
     * @param String $address
     * @return String
     */
    function defangIPaddr($address) 
    {
        $result = '';
        
        for ($i = 0; $i < strlen($address); $i++)   {
            $character = $address[$i];
            
            if ($character === '.') {
                $result .= '[.]';
            } else {
                $result .= $address[$i];   
            }
        }
        
        return $result;
    }
}
}