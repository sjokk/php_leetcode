<?php
use LeetCode\ReverseLinkedList\ListNode;

$input = new ListNode(1);
$input->next = new ListNode(2);
$input->next->next = new ListNode(3);
$input->next->next->next = new ListNode(4);
$input->next->next->next->next = new ListNode(5);