<?php

namespace LeetCode\DiffProductAndSum;

use LeetCode\AbstractSolution;

class Solution extends AbstractSolution
{
    /**
     * @param Integer $n
     * @return Integer
     */
    public function subtractProductAndSum(int $n): int
    {
        $product = 1;
        $sum = 0;

        while ($n > 0) {
            $remainder = $n % 10;

            $product *= $remainder;
            $sum += $remainder;

            $n = intdiv($n, 10);
        }
        
        return $product - $sum;

        // return array_product(str_split($n)) - array_sum(str_split($n));
    }

    public function runTests()
    {
        echo $this->subtractProductAndSum(234);
        echo '<br>';
        echo '<br>';
        
        echo $this->subtractProductAndSum(4421);
        echo '<br>';
        echo '<br>';
    }
}