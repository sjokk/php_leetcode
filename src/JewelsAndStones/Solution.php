<?php

class Solution 
{
    /**
     * @param String $J
     * @param String $S
     * @return Integer
     */
    function numJewelsInStones(string $J, string $S) 
    {
        $numberOfJewels = 0;
        
        for ($i = 0; $i < strlen($S); $i++) {
            for ($j = 0; $j < strlen($J); $j++) {
                if ($S[$i] === $J[$j]) {
                    $numberOfJewels++;
                    break;
                }
            }
        }
        
        return $numberOfJewels;
    }
}